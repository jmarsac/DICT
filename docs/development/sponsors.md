# Financement

```{admonition} Vous êtes prêt à contribuer ?
Le plugin est gratuit pour l'utilisation, pas pour le développement. Si vous l'utilisez de manière intensive ou si vous souhaitez l'améliorer, pensez à contribuer au code, à la documentation ou à financer certains développements :

- [Améliorations proposées](https://oslandia.com/offre-qgis/plugin-de-reponse-aux-dict/)
- Vous souhaitez financer le développement. Merci de [nous envoyer un mail](mailto:qgis@oslandia.com)
```

## Sponsors

Merci à nos sponsors:

```{eval-rst}
.. panels::
    :card: shadow
    :container: container-lg pb-4
    :column: col-lg-4 col-md-4 col-sm-6 col-xs-12 p-2
    :img-top-cls: pl-5 pr-5

    ---
    :img-top: ../../DICT/resources/images/logo_azimut.png

    Azimut
    ^^^^^^

    Jean-Marie Arsac d'Azimut a réalisé la nouvelle version du plugin.

    ++++++
    .. link-button:: https://www.azimut.fr/index.html
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block

    ---
    :img-top: ../../DICT/resources/images/logo_sdev.png

    Syndicat Départemental d'Electricité des Vosges
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Le SDEV a financé la réalisation de la nouvelle version du plugin.

    ++++++
    .. link-button:: http://www.sdev88.fr/
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block

    ---

    Tristan Cazetti et Régis Bouquet
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Tristan Cazetti et Régis Bouquet ont réalisé le portage du plugin sur QGIS 3.

    ++++++

    ---
    :img-top: ../../DICT/resources/images/logo_oslandia.png

    Oslandia
    ^^^^^^^^

    Depuis l'arrivée de Loïc Bartoletti, Oslandia a contribué au développement du plugin sur ses fonds propres puis a mis son expertise au service des besoins exprimés par les utilisateurs finaux.

    ++++++
    .. link-button:: https://oslandia.com/
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block

    ---
    :img-top: ../../DICT/resources/images/logo_megeve.jpg

    Megève
    ^^^^^^

    La commune a financé le développement initial par l'intermédiaire de son employé Loïc Bartoletti.

    ++++++
    .. link-button:: https://mairie.megeve.fr/
        :type: url
        :text: Website
        :classes: btn-outline-primary btn-block
```
